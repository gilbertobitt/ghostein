﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierShadowWalking : MonoBehaviour{

	public int YPositionOdd;
	public int YPositionEven;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update (){
		ChangeTransformPosition(Mathf.RoundToInt(transform.position.x) % 2 == 0 ? YPositionEven : YPositionOdd);
	}

	public void ChangeTransformPosition(int yPos){
		var changePos = transform.position;
		changePos.x = YPositionOdd;
		transform.position = changePos;
	}
}
