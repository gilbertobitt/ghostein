﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using DG.Tweening.Plugins.Options;
using UnityEngine.UI;
using TMPro;

public static class TextMeshDotweenExtensions {

	public static Tweener DOText(this TextMeshProUGUI target, string endValue, float duration, bool richTextEnabled = true, ScrambleMode scrambleMode = ScrambleMode.None, string scrambleChars = null)
	{
		return DOTween.To((DOGetter<string>) (() => target.text), (DOSetter<string>) (x => target.text = x), endValue, duration).SetOptions(richTextEnabled, scrambleMode, scrambleChars).SetTarget<Tweener>((object) target);
	}

}
