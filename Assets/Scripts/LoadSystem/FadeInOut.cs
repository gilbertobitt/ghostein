﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeInOut : MonoBehaviour{

	public Image FaderImage;
	public float FadeDuration = 1f;
	public string LoadLevel;
	public bool alreadyLoading = false;
	public bool isStartScreen = true;

	// Use this for initialization
	void Start (){
		FaderImage.DOFade(0f, FadeDuration).onComplete += DisableImageRaycast;

	}

	public void DisableImageRaycast(){
		if(isStartScreen) return;
		FaderImage.enabled = false;
	}

	public void LoadAsync(){
		if(!isStartScreen) return;
		if (alreadyLoading) return;
		FaderImage.enabled = enabled;
		alreadyLoading = true;
		FaderImage.DOFade(1f, FadeDuration);
		StartCoroutine(LoadYourAsyncScene());

	}
	
	
	
	IEnumerator LoadYourAsyncScene()
	{
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(LoadLevel);

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
	}
}
