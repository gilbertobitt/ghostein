﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPosterOnPosition : MonoBehaviour{

	public Transform ReferencePos;
	public Vector3 PositionInstance;
	public GameObject PrefabCartaz;
	
	// Use this for initialization
	void Start (){
		PositionInstance = ReferencePos.position;
	}

	public void SetPosterOnWall(Sprite spriteCartazChoosen){
		PositionInstance = ReferencePos.position;
		var cartazInstance = Instantiate(PrefabCartaz, PositionInstance, Quaternion.identity, this.transform) as GameObject;
		var spriteCartaz = cartazInstance.GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
		if (spriteCartaz != null) spriteCartaz.sprite = spriteCartazChoosen;
	}
}
