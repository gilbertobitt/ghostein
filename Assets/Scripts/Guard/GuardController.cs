﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class GuardController : MonoBehaviour{

	public Rigidbody2D GuardRigidbody2D;
	public Animator GuardAnimator;
	public bool IsWalking = false;
	public Transform LeftPosition;
	public Transform RightPosition;
	public float WalkingSpeed;
	public float RunningSpeed;
	public Player2DChildController ChildController;
	public bool PlayerInArea = false;
	public bool IsPlayerOnRight;
	public bool IsFacingRight;
	public float DistanceDetection;
	public LayerMask FilterLayerMask;
	public GuardStateMachine CurrentState;
	public SpriteRenderer SpriteRenderIcon;
	public SpriteRenderer SpriteRenderExclamationIcon;
	public AudioSource AudioSourceSystem;

	// Use this for initialization
	void Start () {
		ChildController = FindObjectOfType<Player2DChildController>();
		CurrentState = GuardStateMachine.WalkingLeft;
	}
	
	// Update is called once per frame
	void Update (){

		IsPlayerOnRight = ChildController.transform.position.x < transform.position.x;
		
		var raycastDir = ChildController.transform.position - transform.position;
		var hit = Physics2D.Raycast(transform.position, raycastDir, DistanceDetection, FilterLayerMask);
		if (hit.collider != null){
			Debug.DrawRay(transform.position, hit.point, Color.magenta);
			if (hit.collider.CompareTag("Player-Childs")){
				AudioSourceSystem.volume =
					Mathf.Clamp01(Vector2.Distance(hit.collider.transform.position, this.transform.position));
				if (AudioSourceSystem.isPlaying){
					AudioSourceSystem.Play();
				}
				if (ChildController == null) return;
				if ((!IsFacingRight && IsPlayerOnRight) || ((!IsFacingRight == false && IsPlayerOnRight == false))){
					
					if (!ChildController.isHiding || !ChildController.isInsideBox){
						//Animação de derrota.
						if (!SpriteRenderExclamationIcon.enabled){
							SpriteRenderExclamationIcon.enabled = true;
						}
						if (SpriteRenderIcon.enabled){
							SpriteRenderIcon.enabled = false;
							SpriteRenderIcon.flipX = !IsPlayerOnRight;
						}
						CurrentState = GuardStateMachine.Stoped;
						GuardRigidbody2D.velocity = Vector2.zero;
						GuardAnimator.SetBool("IsGameOver", true);
						Player2DChildController.HasGameOver = true;
						IsWalking = false;	
					}
					else{
						if (!SpriteRenderIcon.enabled){
							SpriteRenderIcon.enabled = true;
							SpriteRenderIcon.flipX = !IsPlayerOnRight;
						}
					}
				} else {
					//Exibir icone de atenção.
					if (!SpriteRenderIcon.enabled){
						SpriteRenderIcon.enabled = true;
						SpriteRenderIcon.flipX = !IsPlayerOnRight;
					}
					//Debug.Log("Player em area");
				}
			}
			else{
				if (AudioSourceSystem.isPlaying){
					AudioSourceSystem.Stop();
				}
			}
		}
		else{
			if (SpriteRenderIcon.enabled){
				SpriteRenderIcon.enabled = false;
			}
			
		}

		if (CurrentState == GuardStateMachine.WalkingLeft){
			if (transform.position.x < LeftPosition.position.x){
				CurrentState = GuardStateMachine.Stoped;
				IsWalking = false;
				GuardRigidbody2D.velocity = Vector2.zero;
				StartCoroutine(ReturnTo(GuardStateMachine.WalkingRight));
			} else if(IsWalking == false) {
				GuardRigidbody2D.velocity = new Vector2(-WalkingSpeed, GuardRigidbody2D.velocity.y);
				IsWalking = true;
			}
		} else if (CurrentState == GuardStateMachine.WalkingRight){
			if (transform.position.x > RightPosition.position.x){
				CurrentState = GuardStateMachine.Stoped;
				IsWalking = false;
				GuardRigidbody2D.velocity = Vector2.zero;
				StartCoroutine(ReturnTo(GuardStateMachine.WalkingLeft));
			} else if(IsWalking == false) {
				IsWalking = true;
				GuardRigidbody2D.velocity = new Vector2(WalkingSpeed, GuardRigidbody2D.velocity.y);
			}
		}
		
		if (GuardRigidbody2D.velocity.x < 0f && IsFacingRight){
			IsFacingRight = false;
			var scale = transform.localScale;
			scale.x = Mathf.Abs(scale.x)*-1f;
			transform.localScale = scale;
		} else if(GuardRigidbody2D.velocity.x > 0f && !IsFacingRight){
			IsFacingRight = true;
			var scale = transform.localScale;
			scale.x = Mathf.Abs(scale.x);
			transform.localScale = scale;
		}
		IsWalking = Mathf.Abs(GuardRigidbody2D.velocity.x) > 0.1f;
		AnimationHandler();
	}

	public IEnumerator ReturnTo(GuardStateMachine StateToReturn){
		yield return new WaitForSeconds(3f);
		CurrentState = StateToReturn;
	}

	private void AnimationHandler(){
		GuardAnimator.SetBool("isWalking", IsWalking);
	}
	
	public enum GuardStateMachine{
		WalkingRight,
		WalkingLeft,
		Stoped,
	}
}
