﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsWalking : MonoBehaviour{

	public bool StairOnRight = true;
	public Collider2D StairCollider2D;
	public Player2DChildController ChildController;
	public Player2DGhost GhostController;
	public bool isActionAlreadyRun = false;
	public BoxCollider2D UpPlataform;
	public bool isPlayerUsingStairs = false;
	public bool isGhosted = false;

	private void Start(){
		ChildController = FindObjectOfType<Player2DChildController>();
		GhostController = FindObjectOfType<Player2DGhost>();
	}

	private void OnTriggerStay2D(Collider2D other){
		if (isPlayerUsingStairs) return;
		if (!other.CompareTag(isGhosted ? "Player-Ghost" : "Player-Childs")) return;
		if (!isGhosted){
			if (ChildController == null) return;
			if (!ChildController.isGoindUpOrDown || isActionAlreadyRun != false) return;
			StairCollider2D.enabled = ChildController.isGoindUpOrDown;
			if (UpPlataform != null){
				UpPlataform.enabled = false;
			}
	
			if (StairOnRight){
				ChildController.SetWalkingVelocity(1f);
			}
			else{
				ChildController.SetWalkingVelocity(-1f);
			}
		} else {
			StairCollider2D.enabled = (Mathf.Abs(GhostController.VerticalAxis) > 0.5f);
			if (UpPlataform != null){
				UpPlataform.enabled = (Mathf.Abs(GhostController.VerticalAxis) < 0.5f);
			}
		}
	}

	private void OnTriggerExit2D(Collider2D other){
		if (!other.CompareTag(isGhosted ? "Player-Ghost":"Player-Childs")) return;
		if (!isGhosted){
			if (ChildController == null) return;
			ChildController.isGoindUpOrDown = false;
			isActionAlreadyRun = false;
			if (UpPlataform != null){
				UpPlataform.enabled = true;
			}
		} else{
			isActionAlreadyRun = false;
			if (UpPlataform == null) return;
			UpPlataform.enabled = (Mathf.Abs(GhostController.VerticalAxis) < 0.5f);
		}
	}
}
