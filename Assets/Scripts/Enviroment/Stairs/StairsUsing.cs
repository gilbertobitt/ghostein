﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsUsing : MonoBehaviour{

	public StairsWalking[] StairsWalkings;
	public bool isGhosted = false;

	private void OnCollisionEnter2D(Collision2D other){
		if (!other.collider.CompareTag(isGhosted ? "Player-Ghost":"Player-Childs")) return;
		SetStairsUsing(true);

	}

	private void OnCollisionExit2D(Collision2D other){
		if (!other.collider.CompareTag(isGhosted ? "Player-Ghost":"Player-Childs")) return;
		SetStairsUsing(false);
	}

	private void SetStairsUsing(bool isUsing){
		foreach (var t in StairsWalkings){
			t.isPlayerUsingStairs = isUsing;
		}
	}
}
