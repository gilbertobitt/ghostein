﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManager : MonoBehaviour{

	public GameOverSystem GameOverManager;
	public bool hasEnded = false;
	public CanvasGroup SkillCanvasGroup;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update (){
		if (!Player2DChildController.HasGameOver) return;
		if (hasEnded) return;
		hasEnded = true;
		StartCoroutine(GameOverTimer());
	}

	public IEnumerator GameOverTimer(){
		yield return new WaitForSeconds(2f);
		SkillCanvasGroup.interactable = false;
		GameOverManager.ShowGameOver();
	}
}
