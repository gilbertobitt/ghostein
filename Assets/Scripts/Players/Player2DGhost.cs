﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player2DGhost : MonoBehaviour{
	public float HorizontalAxis;
	public float VerticalAxis;
	public Rigidbody2D PlayerRigidbody2D;
	public float MaxMovementSpeed = 1f;
	public PhysicsMaterial2D StickyMaterial2D;
	public PhysicsMaterial2D SlipperyMaterial2D;
	public SpriteRenderer PlayerSprite;
	/*public LayerMask FilterLayerMask;
	public Transform SpriteTransform;*/

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update (){
		if (Player2DChildController.HasGameOver){
			HorizontalAxis = 0f;
			VerticalAxis = 0f;
			UpdatePlayerVelocityHorizontal(0f);
		}
		else{
			#if UNITY_STANDALONE
				HorizontalAxis = Input.GetAxis("Horizontal");
				VerticalAxis = Input.GetAxis("Vertical");
			#elif UNITY_ANDROID || UNITY_IOS
				HorizontalAxis = CrossPlatformInputManager.GetAxis("Horizontal");
				VerticalAxis = CrossPlatformInputManager.GetAxis("Vertical");
			#endif
		}

		PlayerSprite.flipX = !(PlayerRigidbody2D.velocity.x > 0f);
		PlayerRigidbody2D.sharedMaterial = ((Mathf.Abs(HorizontalAxis) > 0.01f) || (Mathf.Abs(VerticalAxis) > 0.01f)) ? SlipperyMaterial2D : StickyMaterial2D;
	}

	private void FixedUpdate(){
		if(Player2DChildController.HasGameOver) return;
		UpdatePlayerVelocityHorizontal(HorizontalAxis);
	}

	private void UpdatePlayerVelocityHorizontal(float axisHorizontal){
		var velocityTemp = PlayerRigidbody2D.velocity;
		velocityTemp.x = Mathf.Abs(axisHorizontal) > 0.1f ? MaxMovementSpeed * axisHorizontal:0f;
		PlayerRigidbody2D.velocity = velocityTemp;
	}
}
