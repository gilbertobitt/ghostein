﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenBox : MonoBehaviour {
	
	public Player2DChildController ChildController;

	// Use this for initialization
	void Start () {
		ChildController = FindObjectOfType<Player2DChildController>();
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (!other.CompareTag("Player-Childs")) return;
		if (ChildController == null) return;
		ChildController.isInsideBox = true;
	}

	private void OnTriggerExit2D(Collider2D other){
		if (!other.CompareTag("Player-Childs")) return;
		if (ChildController == null) return;
		ChildController.isInsideBox = false;
	}
}
