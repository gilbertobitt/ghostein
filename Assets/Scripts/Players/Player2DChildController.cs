﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2DChildController : MonoBehaviour {
	
	public float HorizontalAxis;
	public Rigidbody2D PlayerRigidbody2D;
	public float MaxMovementSpeed = 1f;
	public Animator AnimatorChild;
	public bool isWalking = false;
	public bool isCrawling = false;
	public bool isRight = true;
	public bool isGoindUpOrDown = false;
	public bool isHiding = false;
	public static bool HasGameOver = false;
	public bool isInsideBox;
	public PhysicsMaterial2D StickyMaterial2D;
	public PhysicsMaterial2D SlipperyMaterial2D;

	// Use this for initialization
	void Start (){
		AnimatorChild = GetComponent<Animator>();
		PlayerRigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update (){
		if (!HasGameOver){
			isWalking = Mathf.Abs(HorizontalAxis) > 0.1f;
			isRight = !(HorizontalAxis < 0f);
			AnimatorChild.SetBool("isWalking", isWalking);
			AnimatorChild.SetBool("isCrouching", isCrawling);
			AnimatorChild.SetBool("isHidden", isHiding);
			FlipCharacter();
		} else{
			isHiding = false;
			isCrawling = false;
			HorizontalAxis = 0f;
			isHiding = false;
			StopWalking();
		}
	}

	public void SetWalkingVelocity(float axisVelocity){
		if (HasGameOver) return;
		HorizontalAxis = axisVelocity;
		isHiding = false;
	}
	
	public void SetWalkingInCrouch(bool isCrouching){
		if (HasGameOver) return;
		isCrawling = isCrouching;
		isHiding = false;
	}

	public void StopWalking(){
		SetWalkingVelocity(0f);
	}

	public void HideIt(){
		SetWalkingVelocity(0f);
		SetWalkingInCrouch(false);
		isHiding = true;
	}
	
	private void FixedUpdate(){
		if (HasGameOver) return;
		UpdatePlayerVelocityHorizontal(HorizontalAxis);
	}

	public void GoindUporDown(bool isUporDowm){
		if (HasGameOver) return;
		isGoindUpOrDown = isUporDowm;
	}

	private void FlipCharacter(){
		if (HasGameOver) return;
		var localScale = transform.localScale;
		if (isRight){
			localScale.x = Mathf.Abs(localScale.x);
		}
		else{
			localScale.x = Mathf.Abs(localScale.x) * -1f;
		}

		transform.localScale = localScale;
		
	}
	

	private void UpdatePlayerVelocityHorizontal(float axisHorizontal){
		if (HasGameOver) return;
		var velocityTemp = PlayerRigidbody2D.velocity;
		velocityTemp.x = Mathf.Abs(axisHorizontal) > 0.1f ? MaxMovementSpeed*axisHorizontal:0f;
		PlayerRigidbody2D.velocity = velocityTemp;
		PlayerRigidbody2D.sharedMaterial = ((Mathf.Abs(PlayerRigidbody2D.velocity.x) > 0.3f) || (Mathf.Abs(PlayerRigidbody2D.velocity.y) > 0.3f)) ? SlipperyMaterial2D : StickyMaterial2D;
	}


}
