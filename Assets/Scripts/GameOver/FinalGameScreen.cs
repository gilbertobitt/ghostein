﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalGameScreen : MonoBehaviour {

	public FadeInOut FadeInOutGame;
	public Image ImageGameOver;
	public AudioSource MainAudioSource;
	public AudioSource AmbientAudioSource;
	public AudioClip AudioClipGameOver;
	public bool AlreadyStarted = false;
	public bool CanChangeScene;
	public float TimeToWay;
	public GameObject TextToShow;
	public PlayableDirector TextAnimation;
	public PlayableDirector FuracaoAnimation;
	public GameObject[] DeactiveOtherCanvas;

	public void ShowFinalGame(){
		for (int i = 0; i < DeactiveOtherCanvas.Length; i++){
			DeactiveOtherCanvas[i].SetActive(false);
		}
		if(AlreadyStarted) return;
		CanChangeScene = false;
		ImageGameOver.gameObject.SetActive(true);
		AmbientAudioSource.Stop();
		MainAudioSource.Play();
		FuracaoAnimation.Play();
		ImageGameOver.DOFade(1f, 1f);
		StartCoroutine(TimerToShow());
	}

	public IEnumerator TimerToShow(){
		yield return new WaitForSeconds(TimeToWay);
		TextToShow.SetActive(true);
		TextAnimation.Play();
		CanChangeScene = true;
	}

	public void ReturnToMenu(){
		if(!CanChangeScene) return;
		if(AlreadyStarted) return;
		StartCoroutine(LoadYourAsyncScene());
	}
	
	IEnumerator LoadYourAsyncScene()
	{
		// The Application loads the Scene in the background as the current Scene runs.
		// This is particularly good for creating loading screens.
		// You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
		// a sceneBuildIndex of 1 as shown in Build Settings.

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("TitleScreen");

		// Wait until the asynchronous scene fully loads
		while (!asyncLoad.isDone)
		{
			yield return null;
		}
	}
	
}
