﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameOverSystem : MonoBehaviour{

	public FadeInOut FadeInOutGame;
	public Image ImageGameOver;
	public AudioSource MainAudioSource;
	public AudioSource AmbientAudioSource;
	public AudioClip AudioClipGameOver;
	public bool AlreadyStarted = false;

	public void ShowGameOver(){
		if(AlreadyStarted) return;
		ImageGameOver.enabled = true;
		AmbientAudioSource.Stop();
		MainAudioSource.Play();
		ImageGameOver.DOFade(1f, 1f).onComplete += ReturnToMenu;
	}

	public void ReturnToMenu(){
		FadeInOutGame.LoadAsync();
	}


}
