﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour{

	public Camera MainCameraComponent;
	public Transform FollowTransform;

	// Use this for initialization
	void Start (){
		MainCameraComponent = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update (){
		var CurrentPos = FollowTransform.position;
		CurrentPos.z = MainCameraComponent.transform.position.z;
		MainCameraComponent.transform.position =
			Vector3.Lerp(MainCameraComponent.transform.position, CurrentPos, Time.deltaTime);
	}
}
