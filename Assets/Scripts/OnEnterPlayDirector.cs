﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;

public class OnEnterPlayDirector : MonoBehaviour{

	public PlayableDirector Director;

	private void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag("Player-Childs") || other.CompareTag("Player-Ghost")){
			Director.Play();
		}
		
	}
}
