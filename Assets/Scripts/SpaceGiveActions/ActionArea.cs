﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ActionArea : MonoBehaviour{

	public BoxCollider2D TriggerArea;
	public Player2DChildController ChildController;
	public ActionCartazes SettedActionCartazes;

	// Use this for initialization
	void Start (){
		TriggerArea = GetComponent<BoxCollider2D>();
		ChildController = FindObjectOfType<Player2DChildController>();
	}

	private void OnTriggerEnter2D(Collider2D other){
		Debug.Log("trigger");
		if (!other.CompareTag("Player-Childs")) return;
		Debug.Log("Player in the Trigger");
		if (ChildController == null) return;
		switch (SettedActionCartazes){
			case ActionCartazes.StartWalking:
				ChildController.SetWalkingVelocity(1f);
				break;
			case ActionCartazes.WalkingCrounch:
				ChildController.SetWalkingInCrouch(true);
				break;
			case ActionCartazes.Parar:
				ChildController.SetWalkingVelocity(0f);
				break;
			default:
				ChildController.SetWalkingVelocity(0f);
				break;
		}
	}
}
