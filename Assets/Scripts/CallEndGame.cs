﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallEndGame : MonoBehaviour{

	public FinalGameScreen FinalGame;
	
	private void OnTriggerEnter2D(Collider2D other){
		if (!other.CompareTag("Player-Childs")) return;
		FinalGame.ShowFinalGame();
	}
}
