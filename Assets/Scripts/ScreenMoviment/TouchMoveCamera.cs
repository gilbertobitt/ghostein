﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class TouchMoveCamera : MonoBehaviour{
	public Vector3 startPos, deltaPosition;
	public bool directionChosen;
	public Camera MainCameraComponent;


	// Use this for initialization
	void Start(){ }

	// Update is called once per frame
	void Update(){
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved){
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			var currenPos = MainCameraComponent.transform.position;
			currenPos.x += (touchDeltaPosition.x*-1f);
			MainCameraComponent.transform.position = currenPos;
		}

	}
}
