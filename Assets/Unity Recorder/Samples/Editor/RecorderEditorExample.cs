#if UNITY_EDITOR

 namespace UnityEditor.Recorder.Examples
 {
     /// <exclude/>
     public static class RecorderEditorExample
     {
         [MenuItem("Window/General/Recorder/Examples/Start Recording", false, 1100)]
         static void StartRecording()
         {
             var recorderWindow = EditorWindow.GetWindow<RecorderWindow>();
             recorderWindow.StartRecording();
         }
    
         [MenuItem("Window/General/Recorder/Examples/Stop Recording", false, 1100)]
         static void StopRecording()
         {
             var recorderWindow = EditorWindow.GetWindow<RecorderWindow>();
             recorderWindow.StopRecording();
         }
     }
}

#endif
