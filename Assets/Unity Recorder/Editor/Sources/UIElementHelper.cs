﻿using UnityEngine.Experimental.UIElements;

namespace UnityEditor.Recorder
{
    static class UIElementHelper
    {
        // TODO Remove this helper method once 2018.3 is released and can produce a 18.3 Asset Store Package. 
        internal static void SetFlex(VisualElement element, float value)
        {
            #if UNITY_2018_3_OR_NEWER
                element.style.flex = new Flex(value);
            #else
                element.style.flex = value;
            #endif
        }
    }
}